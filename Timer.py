import time
from datetime import datetime, timedelta

timeString = ""

def GetTime(seconds):
    """Convert seconds to a time string "[[[DD:]HH:]MM:]SS".
    """
    dhms = ''
    for scale in 86400, 3600, 60:
        result, seconds = divmod(seconds, scale)
        if dhms != '' or result > 0:
            dhms += '{0:02d}:'.format(result)
    dhms += '{0:02d}'.format(seconds)
    return dhms
	

print("Press enter to go")
#useless = input()

clockAmount = 0

while True:
	clockAmount += 1
	currentTime = GetTime(clockAmount)
	print(currentTime)
	f = open('timefile.txt', 'w')
	f.write(currentTime)
	time.sleep(1)
	
